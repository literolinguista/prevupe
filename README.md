## Pré-vestibular da Universidade de Pernambuco (UPE) - PREVUPE

Atividades lúdicas do cursinho pré-vestibular [PREVUPE](https://www.instagram.com/prev.upe "PREVUPE"){target="_blank"}. 

Para usar em suas aulas sem precisar de conexão com a Internet, basta salvar localmente a pasta chamada [prevupe](https://gitlab.com/literolinguista/prevupe/-/tree/master/public/prevupe?ref_type=heads){target="_blank"}, disponível no repositório.

- [Quiz: Expressões das redes sociais, 2002](prevupe/redes-sociais/index.html "Quiz: Expressões das redes sociais, 2002"){target="_blank"}
- [Quiz: Expressões idiomáticas, 2002](prevupe/expressoes-idiomaticas/index.html "Quiz: Expressões idiomáticas, 2002"){target="_blank"}
- [Quiz: Gírias, 2021](prevupe/girias/index.html "Quiz: Gírias, 2021"){target="_blank"}
