## Prevupe

> “Não sou contra que você entre na universidade, mas não deixe isso atrapalhar seus estudos.” Mark Twain.

### Atividades lúdicas do cursinho pré-vestibular [PREVUPE](https://www.instagram.com/prev.upe "PREVUPE"){target="_blank"}. 

Para usar os quizes sem precisar de conexão com a Internet, basta salvar num pendrive a pastinha completa do tema de sua escolha a partir [deste link](https://gitlab.com/literolinguista/prevupe/-/tree/master/public/prevupe){target="_blank"} e usar no seu computador. Pode modificar, compartilhar, etc... mantendo a [Licença Creative Commons](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR "Licença Creative Commons"){target="_blank"}.

- [Gramática: resumo, 2024](gramatica/index.html "Gramática: resumo, 2024"){target="_blank"}
- [Gramática: revisão (professor), 2024](revisao/index.html "Gramática: revisão (professor), 2024"){target="_blank"}
- [Quiz: Figuras e funções de linguagem, 2023](prevupe/figuras-funcoes/index.html "Quiz: Figuras e funções de linguagem, 2023"){target="_blank"}
- [Quiz: Expressões das redes sociais, 2022](prevupe/redes-sociais/index.html "Quiz: Expressões das redes sociais, 2022"){target="_blank"}
- [Quiz: Expressões idiomáticas, 2022](prevupe/expressoes-idiomaticas/index.html "Quiz: Expressões idiomáticas, 2022"){target="_blank"}
- [Quiz: Gírias, 2021](prevupe/girias/index.html "Quiz: Gírias, 2021"){target="_blank"}

[Voltar à página Atividades...](https://literolinguista.gitlab.io/atividades.html)
