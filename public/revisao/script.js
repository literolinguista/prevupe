const quizContainer = document.getElementById('quiz-container');
const uploadForm = document.getElementById('upload-form');
let acertos = 0;
let erros = 0;

uploadForm.addEventListener('submit', (event) => {
    event.preventDefault();

    const fileInput = document.getElementById('csv-file');
    const file = fileInput.files[0];

    if (file) {
        const reader = new FileReader();

        reader.onload = (event) => {
            const csvData = event.target.result;
            const questions = parseCSV(csvData);

            quizContainer.innerHTML = '';
            questions.forEach(question => {
                createQuestion(question);
            });
        };

        reader.readAsText(file);
    }
});

function parseCSV(csvData) {
    const lines = csvData.split('\n');
    const questions = [];

    lines.forEach(line => {
        const parts = line.split(',');
        if (parts.length >= 3) {
            const question = {
                number: parts[0].trim(),
                text: parts[1].trim(),
                answers: parts.slice(2).map(answer => ({
                    text: answer.endsWith('*') ? answer.slice(0, -1) : answer,
                    correct: answer.endsWith('*'),
                    dontRemember: answer === "Não lembro"
                }))
            };
            questions.push(question);
        }
    });

    return questions;
}

function createQuestion(question) {
    const container = document.createElement('div');
    container.classList.add('container');

    const questionElement = document.createElement('div');
    questionElement.classList.add('question');

    // Verificar se question.number está definido
    if (question.number) {
        questionElement.innerHTML = `<h2>Questão ${question.number}</h2> <h3> ${question.text}</h3>`;
    } else {
        questionElement.innerHTML = `<h2>${question.text}</h2>`; // Se não estiver definido, use apenas o texto
    }

    container.appendChild(questionElement);

    const answerContainer = document.createElement('div');
    answerContainer.classList.add('answer-container');
    question.answers.forEach(answer => {
        const button = document.createElement('button');
        button.classList.add('collapsible');
        button.dataset.correct = answer.correct;
        button.dataset.dontRemember = answer.dontRemember;
        button.textContent = answer.text;

        const content = document.createElement('div');
        content.classList.add('content');
        if (answer.correct) {
            content.classList.add('correct-message');
            content.textContent = 'Resposta certinha!';
        } else if (answer.dontRemember) {
            content.classList.add('dont-remember-message');
            content.textContent = 'Tudo bem, vamos revisar.';
        } else {
            content.classList.add('incorrect-message');
            content.textContent = 'Quase!';
        }

        answerContainer.appendChild(button);
        answerContainer.appendChild(content);

        button.addEventListener('click', () => {
            // Remove a classe 'selected' de todos os botões
            document.querySelectorAll('.collapsible').forEach(button => button.classList.remove('selected'));

            // Adiciona a classe 'selected' ao botão clicado
            button.classList.add('selected');

            // Exibe ou oculta o conteúdo
            if (content.classList.contains('show')) {
                content.classList.remove('show');
            } else {
                content.classList.add('show');
            }

            // Adiciona as classes 'correct' ou 'incorrect' ao botão
            if (button.dataset.correct === 'true') {
                button.classList.add('correct');
                acertos++;
            } else if (button.dataset.dontRemember === 'true') {
                // Não incrementa erros nem acertos
            } else {
                button.classList.add('incorrect');
                erros++;
            }

            // Atualiza o score
            document.getElementById('acertos').textContent = acertos;
            document.getElementById('erros').textContent = erros;
        });
    });

    container.appendChild(answerContainer);
    quizContainer.appendChild(container);
}