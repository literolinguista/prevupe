// Arcordeon

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function () {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}

var translationShown = false;

function displayVerbSentence() {
  var selector = document.getElementById("verb-selector");
  var selectedVerb = selector.value;
  var displayedSentence = document.getElementById("verb-sentence");

  // Inicialmente, exibe apenas a frase em inglês
  displayedSentence.textContent = selectedVerb.split(';')[0];
  translationShown = false;
}

// Verbos auxiliares
let translationShown_aux = false;

function toggleTranslation_aux() {
    var selector_aux = document.getElementById("verb-selector_aux");
    var selectedVerb_aux = selector_aux.value;
    var displayedSentence_aux = document.getElementById("verb-sentence_aux");

    // Alterna entre a frase em inglês e a tradução
    if (!translationShown_aux) {
        displayedSentence_aux.textContent = selectedVerb_aux.split(';')[1];
        translationShown_aux = true;
    } else {
        displayedSentence_aux.textContent = selectedVerb_aux.split(';')[0];
        translationShown_aux = false;
    }
}

// Lista de verbos
function toggleTranslation() {
  var selector = document.getElementById("verb-selector");
  var selectedVerb = selector.value;
  var displayedSentence = document.getElementById("verb-sentence");

  // Alterna entre a frase em inglês e a tradução
  if (!translationShown) {
    displayedSentence.textContent = selectedVerb.split(';')[1];
    translationShown = true;
  } else {
    displayedSentence.textContent = selectedVerb.split(';')[0];
    translationShown = false;
  }
}
